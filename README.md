MATD
====

Jiří Lajčok, LAJ0013

Exercise 01
-----------

_2020-09-23_

Solution: [ex01/dfa.py](https://bitbucket.org/lajcok/matd/src/master/ex01/dfa.py)

Usage: `python3 dfa.py <pattern> <text>`
