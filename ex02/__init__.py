import sys
from functools import reduce


def naive_search(pattern, text):
    """
    Naive Pattern Search Algorithm
    """
    return filter(
        lambda i: reduce(
            lambda a, v: a and v,  # no premature return optimization to stay brief
            (p == t for p, t in zip(pattern, text[i:]))  # char-by-char comparison for demo purposes
        ),
        range(len(text) - len(pattern) + 1)
    )


def kmp_search(pattern, text):
    """
    Knuth-Morris-Pratt Search Algorithm
    """
    lps = [0] + [
        i if reduce(
            lambda a, v: a and v[0] == v[1],
            zip(pattern[:i], pattern[-i:]),
            True
        ) else 0
        for i in range(1, len(pattern))
    ]

    i, j = 0, 0
    while i < len(text):
        # if pattern[j] == text[i]:
        #     i += 1
        #     j += 1
        #
        # if j == len(pattern):
        #     yield i - j
        #     j = lps[j - 1]
        #
        # # mismatch after j matches
        # elif i < len(text) and pattern[j] != text[i]:
        #     # Do not match lps[0..lps[j-1]] characters,
        #     # they will match anyway
        #     if j != 0:
        #         j = lps[j - 1]
        #     else:
        #         i += 1

        if j == len(pattern):
            yield i - len(pattern)
            j = lps[-1]

        if text[i] == pattern[j]:
            i += 1
            j += 1
        else:
            if j > 0:
                j = lps[j - 1]
            else:
                i += 1


def main(pattern, text):
    print(f'pattern:  {pattern}')
    print(f'text:     {text}')
    print()

    print('Naive Pattern Search')
    for i in naive_search(pattern, text):
        print(i)

    print()
    print('Knuth-Morris-Pratt')
    for i in kmp_search(pattern, text):
        print(i)


if __name__ == '__main__':
    _, _pattern, _text = sys.argv
    main(_pattern, _text)
