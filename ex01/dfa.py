# 2020-09-23

import sys


class State:
    def __init__(self, terminal=False, transitions=None, label=None):
        self.terminal = terminal
        self.transitions = transitions or {}
        self.label = label

    def __str__(self):
        return str(self.label)

    def __repr__(self):
        return f'''State({self.label}{', Terminal' if self.terminal else ''})\n''' + ''.join([
            f'    {trans} → State({state})\n'
            for trans, state in self.transitions.items()
        ])

    def traverse(self, word):
        """
        Walk states through given word
        :param word: Word to walk from state
        :return: Yields states with each step
        """
        state = self
        yield state

        for c in word:
            state = state.transitions.setdefault(c, self)
            yield state


class DFA:
    def __init__(self, initial=None):
        self.initial = initial if isinstance(initial, State) else State(label=0)

    def traverse(self, word):
        yield from self.initial.traverse(word)


class SearchDFA(DFA):
    def __init__(self, pattern, alpha=None):
        """
        Initialize DFA for exact pattern matching
        :param pattern: Pattern to set up DFA for
        :param alpha: (optional) alphabet to use, will be inferred from pattern otherwise
        """
        # generate necessary states
        states = [
            State(label=pattern[:i])
            for i in range(len(pattern) + 1)
        ]

        alpha = set(pattern) | set(alpha or {})
        for i, state in enumerate(states):
            # advancing transition
            c = pattern[i] if i < len(pattern) else None
            if c is not None:
                state.transitions[c] = states[i + 1]

            # step-back transitions
            for a in alpha - {c}:
                qa = pattern[:i] + a
                for p in (pattern[:j - 1] for j in range(len(pattern) + 1, 0, -1)):  # prefixes from longest
                    if qa.endswith(p):  # is suffix?
                        state.transitions[a] = states[len(p)]
                        break

        states[0].label = 'ε'
        states[-1].terminal = True
        super().__init__(initial=states[0])
        self.pattern = pattern

    def search(self, text):
        """
        Search pattern in text
        :param text: Text to search in
        :return: Yields start indices of found matches
        """
        for i, state in enumerate(self.traverse(text)):
            if state.terminal:
                yield i - len(self.pattern)


def main(pattern, text):
    dfa = SearchDFA(pattern)
    matches = []
    for match in dfa.search(text):
        matches.append(match)
        print(match)
    return matches


if __name__ == '__main__':
    _, _pattern, _text = sys.argv
    main(_pattern, _text)
